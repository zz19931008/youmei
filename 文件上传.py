# *-* coding# *-* coding: utf-8 *-*

import os
import requests


def batch_upload_file(path='./videos'):
    file_list = os.listdir(path)

    print 'Total {0} files\n'.format(len(file_list))

    for _file in file_list:
        file_path = os.path.join(path, _file)
        files = {
            "file": open(file_path, "rb")
        }
        r = requests.post("http://www.vuumdjuv.com/upload?key=!solomusic@upload$apk", files=files)
        data = r.json()

        if data['code'] == 200:
            print 'File [{0}] upload is success!'.format(_file)
        else:
            print 'File {0} uploae is fail!'.format(_file)

    print '\nUpload is done!'


if __name__  == '__main__':
    # 目录路径根据实际情况来更改
    path = './videos'

    batch_upload_file(path)