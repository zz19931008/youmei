import tornado.web
import tornado.ioloop
import tornado.httpserver

class Indexhandler(tornado.web.RequestHandler):
    """主路由处理类"""
    def get(self):
        '''对应http的get请求方式'''
        self.write("hello kitty")

if __name__ == '__main__':
    app=tornado.web.Application([(r"/",Indexhandler)])

    '''之前是app.listen(8000),修改后'''
    http_server=tornado.httpserver.HTTPServer(app)
    http_server.bind(9000)
    http_server.start()
    tornado.ioloop.IOLoop.current().start()