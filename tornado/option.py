# coding:utf-8
import tornado.web
import tornado.ioloop
import tornado.httpserver
import tornado.options
from tornado import gen
from tornado_swagger import swagger

'''绑定端口'''
tornado.options.define('port',default=8090,type=int,help='runserver')
'''没有绑定端口'''
tornado.options.define('itcast',default=[],type=str,multiple=True,help='itcast')

class Indexhandler(tornado.web.RequestHandler):
    """主路由处理器"""

    @tornado.web.asynchronous
    @gen.coroutine
    @swagger.operation(nickname="get")
    def get(self):
        self.write("hello m")

if __name__ == '__main__':

    tornado.options.parse_command_line()
    print (tornado.options.options.itcast)
    app=tornado.web.Application([(r"/",Indexhandler)])
    http_server=tornado.httpserver.HTTPServer(app)
    http_server.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.current().start()