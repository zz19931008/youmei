# coding:utf-8
import tornado.web
import tornado.ioloop
class Indexhandler(tornado.web.RequestHandler):
    '''主路由处理类'''
    def get(self):
        "对应http的请求方式"
        self.write("hello kitty")


if __name__ == '__main__':
    app=tornado.web.Application([(r"/",Indexhandler),])

    app.listen(8080)
    tornado.ioloop.IOLoop.current().start()