# !/usr/bin/env python
# *-* coding: utf-8 *-*

import requests

class TiebaSpider(object):
    def __init__(self):
        self.tieba_name = input("输入贴吧名字")
        self.start_page = int(input("开始页数"))
        self.end_page = int(input("结束页数"))
        self.base_url = "http://tieba.baidu.com/f"
        self.headers = {"User-Agent":'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    def send_request(self, tieba_parms):
        response = requests.get(self.base_url, headers=self.headers, params=tieba_parms)
        data = response.content
        return data

    def save_data(self, data, page):
        file_pat = 'tieba/'+ str(page) +'.html'
        print('正在抓取{}页..'.format(page))
        with open(file_pat, 'wb') as f:
            f.write(data)

    def run(self):
        # 拼接参数
        for page in range(self.start_page,self.end_page):
            tieba_parms={
                'kw': self.tieba_name,
                'pn': (page-1)*50
            }

            # 发送请求
            data = self.send_request(tieba_parms)
            print(data)
            #
            self.save_data(data,page)


if __name__ == '__main__':
    tool = TiebaSpider()
    tool.run()

