#coding=utf-8

from settings import *

# 一系列需覆盖的内容
DATABASES['default']['NAME'] = 'solomusic'
DATABASES['default']['HOST'] = '127.0.0.1'
DATABASES['default']['USER'] = 'root'
DATABASES['default']['PASSWORD'] = 'chinese'

__REDIS_HOST__ = '127.0.0.1'

# redis配置
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://%s:6379/1" % __REDIS_HOST__,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 100}
            # "PASSWORD": "",
        }
    }
}


