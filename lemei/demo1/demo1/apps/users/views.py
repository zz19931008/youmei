
from rest_framework.response import Response
from .models import User
from rest_framework.views import APIView
from rest_framework import  generics
from . import  serializers





class UsernameCountView(APIView):

    def get(self,request, username ):
        count = User.object.filter(username=username).count()
        return Response({
            'username':username,
            'count':count
        })


class MobileCountView(APIView):
    def get(self,request,mobile):
        count = User.object.filter(mobile=mobile).count()
        return Response({
            'mobile': mobile,
            'count': count
        })


class RegisterView(generics.CreateAPIView):
     """
     创建新用户对象 generics
     """
     # 指定查询集，,当前是创建不需要序列化器
     # 序列化器

     serializers_class = serializers.UserReisterSerializer



