from django.db import models

from django.contrib.auth.models import  AbstractUser

from django.contrib.auth.models import  AbstractUser

class User(AbstractUser):
    # AbstractUser默认提供了：用户名,密码,邮箱...
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    class Meta:
        db_table = 'tb_user'
        verbose_name = '用户'
        verbose_name_plural =verbose_name
