from rest_framework import  serializers
from .models import User
import  re
from django_redis import  get_redis_connection
class UserReisterSerializer(serializers.Serializer):
    # 不使用Modeserializer的原因
    # 多个参数不纯在模型类的属性种
    # 创建对象是需要将密码加密，但是封装的create方法没有做
    # 注册只考虑反序列化

    username = serializers.CharField(
        max_length=20,
        min_length=5,
        error_messages={
            'max_length':'dddd',
            'min_length':'ssss',
        }
    )
    password = serializers.CharField(
        max_length=20,
        min_length=5,
        error_messages={
            'max_length': 'dddd',
            'min_length': 'ssss',}

    )

    sms_code = serializers.IntegerField()
    mobile = serializers.CharField()

    allow = serializers.BooleanField()
    password2 = serializers.CharField(
        max_length=20,
        min_length=5,
        error_messages={
            'max_length': 'dddd',
            'min_length': 'ssss',}

    )
    def validate_mobile(self, value):
        if re.match('^1[3-9]\d{9}',value):
            raise serializers.ValidationError('手机号格式错误')
        if User.objects.filter(mobile=value).count() > 0:
            raise serializers.ValidationError('手机号已经被使用')

        return value

    def validate_username(self, value):
        if User.objects.filter(username=value).count()>0:
            raise serializers.ValidationError('用户已存在')
        return value

    def validate(self,attrs):
        # 密码是否一致
        password = attrs.get('password')
        password2 = attrs.get('password1')
        if password != password2:
            raise  serializers.ValidationError('密码不一致')


        mobile = attrs.get('mobile')
        sms_code = attrs.get('sms_code')
        redis_cli = get_redis_connection("sms")
        sms_code_redis = redis_cli.get('sms_'+mobile)
        if sms_code_redis is None:
            raise serializers.ValidationError("验证码过期")
        if sms_code != int(sms_code_redis):
            raise serializers.ValidationError('验证码错误')


    def validate_allow(self,value):
        if not value:
            raise serializers.ValidationError("必须同意协议")




