from django.apps import AppConfig


class VerificationsConfig(AppConfig):
    name = 'demo1.apps.verifications'
