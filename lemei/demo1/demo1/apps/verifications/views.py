import random

from django_redis import  get_redis_connection
from rest_framework.response import Response
from rest_framework.views import  APIView

from . import  contants
from celery_tasks.sms.tasks import send_sms

class SmsAPIView(APIView):
    def get(self,request,mobile):

        redis_cli = get_redis_connection("sms")

        if redis_cli.get('sms_flag_+mobile'):
            return Response('message')

        sms_code = random.randint(100000,999999)

        redis_pipline = redis_cli.pipeline()
        redis_pipline.setex('sms_' + mobile, sms_code, contants.SMS_EXPIRES, sms_code)
        redis_pipline.setex('sms_flag_+mobile' + mobile, contants.SMS_FLAG_EXPIRES, 1)
        redis_pipline.excute()

        send_sms.delay()
        return Response({'message':"ok"})



