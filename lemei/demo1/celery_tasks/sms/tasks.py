from demo1.utils.ytx_sdk.sendSMS import CCP

from celery_tasks.main import app

@app.task(name='send_sms ')
def send_sms(mobile, sms_code, expires, temp_id):

    CCP.sendTemplateSMS(mobile, sms_code, expires, temp_id )